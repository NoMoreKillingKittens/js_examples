from PIL import Image
import glob, os
from resizeimage import resizeimage

pngs = []
jpgs = []

os.chdir(".")
for file in glob.glob("*.jpg"):
    jpgs.append(file)

for file in glob.glob("*.png"):
    pngs.append(file)

for x in jpgs:
    with Image.open(x) as f:
        dic = resizeimage.resize_thumbnail(f, [200, 200])
        dic.save(x+'.thumb.jpg', f.format)

for x in pngs:
    with Image.open(x) as f:
        dic = resizeimage.resize_thumbnail(f, [200, 200])
        dic.save(x+'.thumb.png', f.format)

